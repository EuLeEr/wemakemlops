# WeMakeMlOps


Методология проекта пока окончательно не определена, будут опробованы:
    Data Science Lifecycle Process
    Либо два репозитория :
        - Репозиторий исследователя по github.flow 
        - Репозиторий дата-инженера по git.flow
## Построение образов для нашего репозитория
    docker build -t dev-env -f dev/Dockerfile .
    docker build -t prod-env -f prod/Dockerfile .
## Запуск контейнеров
    docker run -d --rm -p 8889:8888 dev-env tail -f /dev/null
    docker run -d --rm -p 8880:8888 prod-env tail -f /dev/null
## Запуск jupyter notebook
    docker exec -it <container_id> bash
    